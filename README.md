This is a simple setup to allow me to detect and connect locally or remotely based on
whether I am local or remote with the same ssh command.

Spin this up in docker using
```sh
docker run --name AmILocal -d -p 9090:80 registry.gitlab.com/bertabus/localping
```

and add something like the following to to your ~/.ssh/config, I've chosen a 2 second
timeout, it should have had a chance to connect by then.

```sh
Match host bertserv exec "[[ $(timeout 2 wget -q -O - http://192.168.1.6:9090/AmILocal) == 'Yes' ]]"
  User username
  HostName 192.168.1.6
  Compression yes
  IdentityFile ~/.ssh/id_rsa 

Match host bertserv 
  User username
  HostName www.myDomain.com
  Compression yes
  IdentityFile ~/.ssh/id_rsa 
```

Use for mounting sshfs folders or other purposes inside a bash script with something like.
```sh
if [[ $(timeout 2 wget -qO- "$@" http://192.168.1.6:9090/AmILocal) == 'Yes' ]]
then
	echo "you are local"
else
	echo "you are not local"
fi
#seems to want to create a log file, not sure how to suppress.
rm *wget-log*
```