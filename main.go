package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/AmILocal", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Yes")
	})
	err := http.ListenAndServe(":80", nil) // set listen port
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
